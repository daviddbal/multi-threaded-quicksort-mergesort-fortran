This repository contains 2 folders Merge Sort and Quick Sort.
Use merge sort if stability is important (order of equal values will be preserved).
Use quick sort if stability is not important.  Quick sort is faster.

All versions use a combination of either merge sort or quick sort for large sort chunks.  When the chuck becomes small, about 50 or less elements, insertion sort is used instead.  This produces a significant execution speed increase.

Merge Sort contains 5 folders
MTSort_MDB: Multi-threaded merge sort for derived data types.  Includes multi-threaded sort and multi-threaded 2nd stage merge.
MTSort_MDS: Simplier Multi-threaded merge sort for derived data types.  Includes multi-threaded sort and  single-threaded 2nd stage merge.
MTSort_MRB: Multi-threaded merge sort for Real data.  Includes multi-threaded sort and multi-threaded 2nd stage merge.
MTSort_MRS: Simplier Multi-threaded merge sort for Real data.  Includes multi-threaded sort and  single-threaded 2nd stage merge.
MTSort_MDNB: Multi-threaded merge sort for derived data types without a barrier for synchrizing between sort and merge step (merges chunks when its partner is available instead of waiting until all chunks are sorted to start merging).  This was suppose to be faster than the non-barrier version, but I don't think it is.  I would consider this version experimental.

Quick Sort contains 4 folders
MTSort_QDB: Multi-threaded quick sort for derived data types.  Includes multi-threaded sort and multi-threaded 2nd stage merge.
MTSort_QDS: Simplier Multi-threaded quick sort for derived data types.  Includes multi-threaded sort and  single-threaded 2nd stage merge.
MTSort_QRB: Multi-threaded quick sort for Real data.  Includes multi-threaded sort and multi-threaded 2nd stage merge.
MTSort_QRS: Simplier Multi-threaded quick sort for Real data.  Includes multi-threaded sort and  single-threaded 2nd stage merge.

COMPILING MULTI-THREADED OPENMP FORTRAN CODE

Instructions are for Linux.  Windows is probably similar, but I haven't tested it.

Intel Fortran
Eclipse: Right-click on the project and to go Properties.  Expand Fortran Build.  Go to Settings.  Under the Tool Settings tab, under the Intel Fortran Compiler menu select Language.  In the pull-down menu next to Process OpenMP Directives select Generate Parallel Code (-openmp).  Under the Intel Fortran Linker menu select Command Line.  In the Additional Options field type -openmp.

Command Line: To compile the MTSort_QRB.f90 file type the following in a terminal window:
ifort MTSort_QRB.f90 -oSort -O2 -openmp
The result is a executable named Sort

GNU Fortran
Eclipse: Right-click on the project and to go Properties.  Expand Fortran Build.  Go to Settings.  Under the Tool Settings tab, under the GNU Fortran Compiler menu select Miscellaneous.  In the Other flags field add -fopenmp.  Under the GNU Fortran Linker menu select Miscellaneous.  In the Linker flags field type -fopenmp.

Command Line: To compile the MTSort_QRB.f90 file type the following in a terminal window:
gfortran MTSort_QRB.f90 -O3 -oSort2 -fopenmp
The result is an executable named Sort2

Note: The multi-threaded sort test programs from BalFortran.org produce different results in GNU Fortran and Intel Fortran due to different random number generators.  When given identical data sets both GNU Fortran and Intel Fortran produce identical results.