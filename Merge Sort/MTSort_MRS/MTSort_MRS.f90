! Simple OpenMP multi-threaded stable sort.
! Sorts real arrays
! Sorts equal parts of the input array on N number of threads.
! Uses both merge sort & insertion sort.  In merge sort, when the array size is less than a defined limit insertion sort is used
! instead, as it is faster for small arrays.
! After the initial sort, it does a bottom-up merge sort to combine the sorted chunks into a single sorted list.  The code will
! merge multiple chunks are once, but only uses a single-threaded merge.  That means less that all the available threads will be
! used to merge the chunks.  For example, when run with 4 threads all 4 will be used to sort 4 chunks.  Then 2 threads will merge
! the 4 chunks to 2 larger chunks.  Finally, 1 thread will merge the 2 chunks together.  See my better multi-threaded sort for a
! more efficient algorithm that uses all available threads during the merge process.
! Only uses power of 2 number of threads (2, 4, 8, etc).  Uses 50% more memory than array A as temp storage.
! Note: Subroutines use explicit size arrays instead of allocatable arrays.  The code runs faster this way.  Segmentation fault
! may occur for large arrays (i.e. 1 billion+ elements).  If a problem occurs either change to allocatable arrays or set memory
! stack size to unlimited.  In Linux the BASH command is "ulimit -s unlimited"
module sort

use, intrinsic :: iso_fortran_env, only: int64, real32, int32

implicit none

public :: MTSort
private :: MergeSort8a, MergeSort8d, InsertionSortA, InsertionSortD, Merge8A, Merge8D

contains

! Main sorting subroutine - merge sort and insertion sort - multi-threaded
subroutine MTSort(A, nA, direction, nt)

! USED MODULES
use, intrinsic :: iso_fortran_env, only: int64, real32, real64
use omp_lib

! DUMMY ARGUMENTS
! nA:  size of input array a
! a: input data to be sorted
! direction: A = ascending, D = descending
! nt: number of threads, if omitted then uses all available
integer (kind=int64), intent(in) :: nA
real (real32), dimension(nA), intent(in out) :: A
character, intent(in) :: direction
integer, optional, intent(in) :: nt

! LOCAL VARIABLES
! nt1: number of threads available.  Either a copy of input variable nt or omp_get_max_threads() if nt is not present.
! nt2: largest power of 2 number of threads (i.e. 2,4,8,16...)
! t: thread number
! s, f: Chunk indexes (start and finish) for each thread used by the initial quicksort/insertion sort combo.
! i, j: loop counters to merge sorted chunks together in order For example, with 4 threads first merge chunks 1 & 2 and 3 & 4 at the
!       same time.  Next merge the previously made 1+2 chunk and 3+4 chunk to generate the completely sorted result.
! levels: Log(base 2) of nt2 (i.e 8 threads has 3 levels 2**3).  Extent of outer merge loop (level_loop).
! step: number of threads available for each merge.
! gap: number of pieces between left and right chunks to be merged.
! span: number of pieces in each left and right chunk.  As level_loop progresses each left and right chunk spans multiple chunks.
! l1, l2, r1, r2: index locations of left and right chunks to be merged.  1 is start, 2 is finish
! left_part: temp array half the size of chunks being merged
! i_limit: array size limit when quicksort changes to insertion sort.  50 is a good value for small data types.  As derived
!               derived type increases in bytes the i_limit should become lower.
! verbose: T means to output messages, F means to output no messages
integer :: nt1
integer :: nt2
integer :: t=1
integer (kind=int64), dimension(:), allocatable :: s, f
integer :: i, j
integer :: levels
integer :: step
integer :: gap
integer :: span
integer (kind=int64) :: l1, l2, r1, r2
real (real32), dimension(:), allocatable :: left_part    ! temp array for left half of array to be sorted
integer, parameter :: i_limit = 50
logical, parameter :: verbose = .true.

! ABSTRACT INTERFACE (used by procedure pointer)
abstract interface ! explicit interface block to make sure procedure pointers are given correct arguments

    subroutine MergeSort_Interface(A,nA,T,nT,i_limit)
        use, intrinsic :: iso_fortran_env, only: int64, real32
        integer (kind=int64), intent(in) :: nA
        real (kind=real32), dimension(nA), intent(in out) :: A
        integer(kind=int64), intent(in) :: nT
        real (real32), dimension(nT), intent(out) :: T
        integer, intent(in) :: i_limit
    end subroutine MergeSort_Interface

    subroutine Merge_Interface(A,nA,B,nB,C,nC)
        use, intrinsic :: iso_fortran_env, only: int64, real32
        integer (kind=int64), intent(in) :: nA, nB, nC
        real (real32), dimension(nA), intent(in) :: A
        real (real32), dimension(nB), intent(in) :: B
        real (real32), dimension(nC), intent(out) :: C
    end subroutine Merge_Interface

end interface

! PROCEDURE POINTER
procedure (MergeSort_Interface), pointer :: MergeSort  ! which sort to use (ascending or descending)
procedure (Merge_Interface), pointer :: Merge8         ! which merge to use (ascending or descending)

    ! POINT TO CORRECT SORT & MERGE PROCEDURES
    if (direction == "A" .or. direction == "a") then
        MergeSort => MergeSort8a
        Merge8 => Merge8A
    else if (direction == "D" .or. direction == "d") then
        MergeSort => MergeSort8d
        Merge8 => Merge8D
    else
        write (*,*) "ERROR: Invalid sort direction: ", direction
        write (*,*) "Valid options are A and D for ascending and descending sort"
        write (*,*) "Can not sort."
        return
    endif

    ! FIND AVAILABLE THREADS
    nt1 = 1  ! default to 1 thread in not using openmp
    !$ nt1 = omp_get_max_threads()  ! get max threads available if using OpenMP
    if (nt1 == 1) then
        if (verbose) then
            write (*,*) "WARNING: Multi-threaded sort requested, but either system has only one CPU core or OpenMP is not enabled."
        end if
    end if
    if (present(nt)) then
         nt1 = nt
    end if

    multithread: if (nA < 100000 .or. nt1 == 1) then    ! run single threaded if specified or too little work to have benefit

        ! Single-threaded
        if (verbose) write (*,*) "Single threaded"
        allocate (left_part((nA+1)/2))
        call MergeSort(A, nA, left_part, (nA+1)/2, i_limit)

    else multithread

        ! PARALLEL MERGE SORT
        nt2 = 2 ** int(log(real(nt1))/log(2.0)) ! get largest power of 2 number of threads (i.e. 2,4,8,16...)
        if (verbose) then
            write (*,"(A,I3)") "Threads used =", nt1
            if (nt2 /= nt1) write (*,"(A,I3,a)") "Only efficiently using",nt2," threads."
        end if
        allocate (s(nt2),f(nt2))

        ! SORT PIECES
        !$omp parallel &
        !$omp num_threads(nt2) &
        !$omp private(t, left_part) &
        !$omp shared(a, s, f, nt2, nA)
            !$ t = omp_get_thread_num() + 1 ! add 1 to make first thread 1 instead of 0
            s(t) = nA * (t-1) / nt2 + 1     ! start
            f(t) = nA * t / nt2             ! finish
            allocate (left_part((nA+1)/2))
            call MergeSort(a(s(t):f(t)), &  ! section to be sorted
            &               f(t)-s(t)+1, &  ! size of section
            &               left_part, &    ! temp or output array, (temp if swap_flag = F, output if swap_flag = T)
            &               (nA+1)/2, &     ! size of output array
            &               i_limit)        ! insertion sort limit
            deallocate (left_part)
        !$omp end parallel

        ! MERGE SORTED PIECES
        levels = log(real(nt2))/log(2.0)
        level_loop: do i = levels, 1, -1
            step = 2 ** (levels - i + 1)
            gap = 2 ** (levels - i)
            span = 2 ** (levels - i) - 1

            !$omp parallel &
            !$omp num_threads(nt2) &
            !$omp private (l1, l2, r1, r2, left_part) &
            !$omp shared (a, s, f, nt2, gap, span, step)
            allocate (left_part(f(ceiling(real(step)/2.0))+1)) ! allocate left_part to max size of first half of pieces
            !$omp do
            merge_loop: do j = 1, nt2, step

                l1 = s(j)
                l2 = f(j+span)
                r1 = s(j+gap)
                r2 = f(j+gap+span)
                left_part(1:l2-l1+1) = a(l1:l2)
                call Merge8(left_part(1:l2-l1+1), &     ! left part
                &           l2-l1+1, &                  ! size of left part
                &           a(r1:r2), &                 ! right part
                &           r2-r1+1, &                  ! size of right part
                &           a(l1:r2), &                 ! output array
                &           r2-l1+1)                    ! size of output array
            enddo merge_loop
            !$omp end do
            deallocate (left_part)
            !$omp end parallel
        enddo level_loop

    endif multithread

end subroutine MTSort




! Ascending merge sort with insertion sort as a finisher
recursive subroutine MergeSort8a(A,nA,T,nT,i_limit)

! USED MODULES
use, intrinsic :: iso_fortran_env, only: int64, real32

! DUMMY ARGUMENTS
integer(kind=int64), intent(in) :: nA               ! size of array A
real (real32), dimension(nA), intent(in out) :: A    ! array data to be sorted
integer(kind=int64), intent(in) :: nT               ! size of array T
real (real32), dimension(nT), intent(out) :: T       ! temp array that is half the size of A
integer, intent(in) :: i_limit                      ! maximum array size to be sorted with insertion sort instead of merge sort
! note: i_limit of 26 is a good value for 16 byte derived types, larger types may be best with small values.

! LOCAL VARIABLES
integer(kind=int64) :: nA2,nT2
real (real32) :: temp
integer(kind=int64) :: i, j

    if (nA <= i_limit) then
        call InsertionSortA(A,nA)       ! INSERTION SORT FOR SMALL CHUNK
        return
    end if

   nA2=(nA+1)/2
   nT2=nA-nA2

   call MergeSort8a(A,nA2,T,nT,i_limit)
   call MergeSort8a(A(nA2+1:),nT2,T,nT,i_limit)

   if (A(nA2) > A(nA2+1)) then
      T(1:nA2)=A(1:nA2)
      call Merge8a(T,nA2,A(nA2+1:),nT2,A,nA)
   end if

end subroutine MergeSort8a

! Ascending merge (merges 2 ascending sorted lists into 1 ascending sorted list)
subroutine Merge8a(A, nA, B, nB, C, nC)

! USED MODULES
use, intrinsic :: iso_fortran_env, only: int64, real32

! DUMMY ARGUMENTS
integer(kind=int64), intent(in) :: nA, nB, nC         ! Size of arrays.  Normal usage: nC = nA+nB
! ***NOTE: USING EXPLICIT SHAPE DUMMY ARRAYS AS SHOW BELOW SOMETIMES CAUSES SEGMENTATION FAULTS WITH LARGE ARRAY SIZES
! Explicit shape arrays are faster than allocatable arrays, but if problems occur first try to increase the stack memory size.
! For Linux in terminal enter "ulimit -s unlimited" to set stack size to unlimited.  If problems still occur then switch to above
! allocatable variable declarations and uncomment allocate statement below.
real (real32), dimension(nA), intent(in) :: A  ! left part
real (real32), dimension(nB), intent(in) :: B  ! right part
real (real32), dimension(nC), intent(out) :: C ! output array
!   real (real32), dimension(:), intent(in) :: A
!   real (real32), dimension(:), intent(in) :: B
!   real (real32), dimension(:), intent(out) :: C
! Note: Under normal usage array A is a copy of the left part of array A.  B is the right part of array C (nA+1:nC) meaning B and C
! share the same memory.  Array A must be a copy to insure the merge doesn't write over, but B doesn't need to be a copy.

! LOCAL VARIABLES
integer(kind=int64) :: i, j, k

!    allocate (A(nA),B(nB),C(nC)    ! use variable allocation if necessary to avoid segmentation fault for large arrays.
    i = 1; j = 1; k = 1
    do
        if (i > nA .or. j > nB) exit
        if (a(i) <= b(j)) then
            c(k) = a(i)
            i = i + 1
        else
            c(k) = b(j)
            j = j + 1
        endif
        k = k + 1
    enddo
    if (i <= nA) then
        c(k:) = a(i:)
        return
    endif
    if (j <= nB) c(k:) = b(j:)    ! This statement is only necessary for multi-threaded merge

end subroutine Merge8a

subroutine InsertionSortA(A,nA)
! USED MODULES
use, intrinsic :: iso_fortran_env, only: int64, real32

! DUMMY ARGUMENTS
integer (kind=int64), intent(in) :: nA
real (kind=real32), dimension(nA), intent(in out) :: A

! LOCAL VARIABLES
real (kind=real32) :: temp
integer (kind=int64) :: i, j

    outter: do i = 2, nA
        j = i - 1
        temp = A(i)
        inner: do
            if (j == 0) exit inner
            if (a(j) <= temp) exit inner
            A(j+1) = A(j)
            j = j - 1
        end do inner
        a(j+1) = temp
    end do outter

end subroutine InsertionSortA






! Descending merge sort with insertion sort as a finisher
recursive subroutine MergeSort8d(A, nA, T, nT, i_limit)

! USED MODULES
use, intrinsic :: iso_fortran_env, only: int64, real32

! DUMMY ARGUMENTS
integer(kind=int64), intent(in) :: nA               ! size of array A
real (real32), dimension(nA), intent(in out) :: A    ! array data to be sorted
integer(kind=int64), intent(in) :: nT               ! size of array T
real (real32), dimension(nT), intent(out) :: T       ! temp array that is half the size of A
integer, intent(in) :: i_limit                      ! maximum array size to be sorted with insertion sort instead of merge sort
! note: i_limit of 26 is a good value for 16 byte derived types, larger types may be best with small values.

! LOCAL VARIABLES
integer(kind=int64) :: nA2,nT2
real (real32) :: temp
integer(kind=int64) :: i, j

    if (nA <= i_limit) then
        call InsertionSortD(A,nA)       ! INSERTION SORT FOR SMALL CHUNK
        return
    end if

   nA2=(nA+1)/2
   nT2=nA-nA2

   call MergeSort8d(A,nA2,T,nT,i_limit)
   call MergeSort8d(A(nA2+1:),nT2,T,nT,i_limit)

   if (A(nA2) < A(nA2+1)) then
      T(1:nA2)=A(1:nA2)
      call Merge8d(T,nA2,A(nA2+1:),nT2,A,nA)
   end if

end subroutine MergeSort8d

! Descending merge (merges 2 descending sorted lists into 1 descending sorted list)
subroutine Merge8d(A, nA, B, nB, C, nC)

! USED MODULES
use, intrinsic :: iso_fortran_env, only: int64, real32

! DUMMY ARGUMENTS
integer(kind=int64), intent(in) :: nA, nB, nC         ! Size of arrays.  Normal usage: nC = nA+nB
! ***NOTE: USING EXPLICIT SHAPE DUMMY ARRAYS AS SHOW BELOW SOMETIMES CAUSES SEGMENTATION FAULTS WITH LARGE ARRAY SIZES
! Explicit shape arrays are faster than allocatable arrays, but if problems occur first try to increase the stack memory size.
! For Linux in terminal enter "ulimit -s unlimited" to set stack size to unlimited.  If problems still occur then switch to above
! allocatable variable declarations and uncomment allocate statement below.
real (real32), dimension(nA), intent(in) :: A  ! left part
real (real32), dimension(nB), intent(in) :: B  ! right part
real (real32), dimension(nC), intent(out) :: C ! output array
!   real (real32), dimension(:), intent(in) :: A
!   real (real32), dimension(:), intent(in) :: B
!   real (real32), dimension(:), intent(out) :: C
! Note: Under normal usage array A is a copy of the left part of array A.  B is the right part of array C (nA+1:nC) meaning B and C
! share the same memory.  Array A must be a copy to insure the merge doesn't write over, but B doesn't need to be a copy.

! LOCAL VARIABLES
integer(kind=int64) :: i, j, k

    i = 1; j = 1; k = 1
    do
        if (i > nA .or. j > nB) exit
        if (a(i) >= b(j)) then
            c(k) = a(i)
            i = i + 1
        else
            c(k) = b(j)
            j = j + 1
        endif
        k = k + 1

    enddo
    if (i <= nA) then
        c(k:) = a(i:)
        return
    endif
    if (j <= nB) c(k:) = b(j:)    ! THIS STATEMENT IS NOT NECESSARY IN SINGLE THREADED MERGE

end subroutine Merge8d

subroutine InsertionSortD(A,nA)
! USED MODULES
use, intrinsic :: iso_fortran_env, only: int64, real32

! DUMMY ARGUMENTS
integer (kind=int64), intent(in) :: nA
real (kind=real32), dimension(nA), intent(in out) :: A

! LOCAL VARIABLES
real (kind=real32) :: temp
integer (kind=int64) :: i, j

    outter: do i = 2, nA
        j = i - 1
        temp = A(i)
        inner: do
            if (j == 0) exit inner
            if (a(j) >= temp) exit inner
            A(j+1) = A(j)
            j = j - 1
        end do inner
        a(j+1) = temp
    end do outter

end subroutine InsertionSortD

end module sort




! PROGRAM TO TEST SORT MODULE
program test_sort
! USED MODULES
use sort

implicit none

integer (kind=int64), parameter :: nA =  100000000
real (real32), allocatable, dimension(:) :: A
integer :: count1, count2, rate
integer, dimension(12) :: seed = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]

    call random_seed(put = seed)
    allocate (A(nA))
    call make_data(A,nA)
    call system_clock(count1,rate)
    call MTSort(A,nA,"Ascending")   ! Missing optional 4th argument means use all available threads.  To specify, add 4th argument.
    call system_clock(count2)

    write (*,*) "First and last in sorted list"
    write (*,*) A(1), A(nA)
    write (*,*) "Execution time in seconds:"
    write (*,*) real(count2-count1)/real(rate)

contains

subroutine make_data(A,nA)
! USED MODULES
use, intrinsic :: iso_fortran_env, only: int64, real32

! DUMMY ARGUMENTS
integer (kind=int64), intent(in) :: nA
real (kind=real32), dimension(nA), intent(out) :: A

! LOCAL VARIABLES
integer (kind=int64) :: i
real :: random

    do i = 1, nA
        call random_number(random)
        A(i) = random
    end do

end subroutine make_data
end program test_sort
