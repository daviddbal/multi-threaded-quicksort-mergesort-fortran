! Better OpenMP multi-threaded sort.
! Sorts derived data types
! No barrier between sort and merge steps.

! Sorts equal parts of the input array on N number of threads.
! Uses both quicksort & insertion sort.  In quicksort, when the array size is less than a defined limit insertion sort is used
! instead, as it is faster for small arrays.
! After the initial sort, it does a bottom-up merge sort to combine the sorted chunks into a single sorted list.  The code will
! merge multiple chunks are once, and uses multiple threads for each merge.  For example, when run with 4 threads all 4 will be
! used to sort 4 chunks.  Then each set of 2 chunks will use 2 threads to merge together to form  2 larger chunks.  Finally,
! 4 threads will merge the 2 chunks together.
! Optimally uses power of 2 number of threads (2, 4, 8, etc).  The initial sort can use non-power of 2 threads, but not the merge.
! Uses double the memory of input array A as temp storage.
! Note: Subroutines use explicit size arrays instead of allocatable arrays.  The code runs faster this way.  Segmentation fault
! may occur for large arrays (i.e. 1 billion+ elements).  If a problem occurs either change to allocatable arrays or set memory
! stack size to unlimited.  In Linux the BASH command is "ulimit -s unlimited"
module types

use, intrinsic :: iso_fortran_env, only: int64, real32, int32

type group
    real (kind=real32) :: value
    integer (kind=int64) :: order
end type group

end module types

module sort

use, intrinsic :: iso_fortran_env, only: int64

implicit none

    type queue
        integer (kind=int64) :: s1, f1, s2, f2      ! start and finish locations of groups to be merged
        integer :: id                               ! group id
        integer :: level                            ! level in binary tree of merging
        logical :: AtoA2                            ! T means copy from array A to array A2.  F means copy from array A2 to array A
        type (queue), pointer :: next
    end type queue

    type stack
        integer :: t
        type (stack), pointer :: next
    end type stack

public :: mSort8e
!private ::

contains

! THIS ONE PERFORMS BEST OF NO BARRIER VERSIONS
! USES LINKED LIST OF MERGE GROUPS
! WARNING: DOES NON-STANDARD CONFORMING SETTING LOCKS OUTSIDE REGIONS
! ADD MORE EFFICIENCY BY ELIMINATING BARRIERS AND MERGING SORTED GROUPS AS SOON AS THEY ARE MADE.
! ALSO MAKE 1ST STAGE MERGESORT MANY MORE GROUPS THAN THE NUMBER OF THREADS - TRY 128 GROUPS PER THREAD
! ALSO MAKE IT REQUIRED FOR THAT PAIRED GROUPS EXIST BEFORE MERGING AND PUT RESULTS IN SAME PLACE IN COPY.  THIS ENABLES ME TO ONLY USE 1 COPY OF ARRAY A
! Multi-thread merge sort.  Optimized for 4, 16, 64, etc. threads.  There is a small penality for 2, 8, 32, etc. threads due to an
! extra memory copy.  Poorly uses non-power of 2 number of threads.  Makes a copy of input array a as it sorts so it needs double
! the memory used by array a.
subroutine mSort8e(A,array_size,direction,nt,groups_per_thread )

! USED MODULES
use, intrinsic :: iso_fortran_env, only: int64, real64
use types
use omp_lib, only: omp_get_max_threads, omp_get_thread_num, omp_lock_kind, omp_init_lock, omp_set_lock, omp_unset_lock, &
&                  omp_test_lock

! DUMMY ARGUMENTS
! array_size:  size of input array a
! a: input data to be sorted
! direction: A = ascending, D = descending
! nt: number of threads, if omitted then uses all available

!integer (kind=int64), intent(in) :: array_size
!type (group), dimension(array_size), intent(in out) :: a

integer (kind=int64), intent(in) :: array_size
type (group), dimension(array_size), intent(in out) :: A

character, intent(in) :: direction
integer, optional, intent(in) :: nt

! LOCAL VARIABLES
! nt1: number of threads available.  Either a copy of input variable nt or omp_get_max_threads() if nt is not present.
! nt2: largest power of 2 number of threads (i.e. 2,4,8,16...)
! nt3: Total number of work groups in each 1st level mergesort step.  Exists to improve load balancing.  128 groups per nt2 thread
!      seems to work well.
! nt_extra: extra threads to use in last merge (is >0 if nt is not a power of 2)
! t: thread number
! s, f: start and finish indexes into array a identifying chunks for each thread
! temp_copy: temp array half the size of chunk.  Used for storing left part during sort to prevent overwrites.

! a2: copy of a where sorted results are copied in stages.  Progressively sorted output toggle between array a and a2.  Fuly sorted
!     output ends in array a when finished.
! i, j: loop counters to merge sorted chunks together in order (i.e. 1 & 2, 3 & 4, 1+2 & 3+4)
! levels: Log(base 2) of nt2 (i.e 8 threads has 3 levels 2**3).  Extent of outer merge loop (level_loop).
! step: number of threads available for each merge
! gap: number of pieces between left and right chunks to be merged
! span: number of pieces in each left and right chunk.  As level_loop progresses each left and right chunk spans multiple chunks.
! l1, l2, r1, r2: index locations of left and right chunks to be merged.  1 is start, 2 is finish
! swap_flag: flag used to toggle between array a and array a2
! s2, f2: start and finish of sections of array a2 to be copied into array a
integer :: nt1
integer :: nt2
integer :: nt3
integer :: nt4
integer :: nt_extra
integer :: t=1
integer (kind=int64), dimension(:), allocatable :: s, f
type (group), dimension(:), pointer :: temp_copy
!type (group), dimension(array_size) :: a2  ! using an automatic array here causes segmentation fault with large array_size
type (group), dimension(:), allocatable :: a2
integer :: i, j
integer :: levels
integer :: step
integer :: gap
integer :: span
!integer (kind=int64) :: l1, l2, r1, r2, o1, o2
integer (kind=int64) :: s1, s2, f1, f2  ! start and finish of groups to be merged
integer (kind=int64) :: s1_next, s2_next, f1_next, f2_next  ! start and finish of next group to be merged
integer :: f_next_index
integer (kind=int64) :: max_group
logical :: swap_flag
!integer (kind=int64) :: s2, f2
integer :: ii
real (kind=real64) :: part                                      ! size of each piece of A for each thread
!integer (kind=int64), allocatable, dimension(:,:) :: s2, f2      ! start and finish of each group being sorted
!logical, allocatable, dimension(:,:) :: synch       ! flags indicating when groups can be merged
!logical, allocatable, dimension(:) :: release       ! flag when T means thread has no work and should be released
integer :: available                         ! number of group paird waiting to be merged
integer, allocatable, dimension(:) :: next_counter, next_counter2
integer, allocatable, dimension(:) :: need
!integer (kind=int64), allocatable, dimension(:,:,:) :: ls, lf, rs, rf
!integer, allocatable, dimension(:,:) :: id      ! sequential numbering of merge groups.  First group done is assigned 1, second 2, etc. regardless of which thread did the sorting or where in array A the group is.
integer, allocatable, dimension(:) :: counter
integer :: temp_counter     ! private copy of next counter for each thread.  Ensures proper counter order even if chunks are finished in different orders than they begin.
integer, allocatable, dimension(:,:) :: order   ! order in which chunks are merged.  First dimension is thread number, second is level
integer, allocatable, dimension(:) :: t1
integer (kind=omp_lock_kind) :: merge_stack
integer (kind=omp_lock_kind) :: thread_stack
integer (kind=omp_lock_kind), allocatable, dimension(:) :: tlock
integer :: n, n2
integer :: count1, count2
logical(1), allocatable, dimension(:,:) :: done ! T when the merge chunk is finished.
integer :: thread_count
!integer, parameter :: groups_per_thread = 32
integer, intent(in) :: groups_per_thread
type (queue), pointer :: head, tail, temp_pointer
type (stack), pointer :: t_head, t_tail, t_temp_pointer ! linked list of waiting threads
logical :: all_done
integer :: level
integer :: id, id_new
logical :: AtoA2
logical :: go
logical :: flag     ! result of setting tlock

! PROCEDURE POINTER
procedure (MergeSort_Interface), pointer :: MergeSort8  ! which sort to use (ascending or descending)
procedure (Merge_Interface), pointer :: Merge8          ! which merge to use (ascending or descending)

! ABSTRACT INTERFACE (used by procedure pointer)
abstract interface ! explicit interface block to make sure procedure pointers are given correct arguments

    subroutine MergeSort_Interface(A,n,T,n2)
        use types
        use, intrinsic :: iso_fortran_env, only: int64
        integer(kind=int64), intent(in) :: N
        type (group), dimension(N), intent(in out) :: A
        integer(kind=int64), intent(in) :: n2               ! size of array T
        type (group), dimension(n2), intent(out) :: T
    end subroutine MergeSort_Interface

    subroutine Merge_Interface(a,nA,b,nB,c,nC,nt)
        use types
        use, intrinsic :: iso_fortran_env, only: int64
        integer (kind=int64), intent(in) :: nA, nB, nC
        type (group), dimension(nA), intent(in) :: A
        type (group), dimension(nB), intent(in) :: B
        type (group), dimension(nC), intent(out) :: C
        integer, intent(in) :: nt
    end subroutine Merge_Interface

end interface

!!$ call omp_set_nested(.true.)

    ! POINT TO CORRECT SORT PROCEDURE
    if (direction == "A" .or. direction == "a") then
        MergeSort8 => MergeSort8ao
!        MergeSort8n => MergeSort8an
        Merge8 => Merge8a_mt
    else if (direction == "D" .or. direction == "d") then
        MergeSort8 => MergeSort8do
!        MergeSort8n => MergeSort8dn
        Merge8 => Merge8d_mt
    else
        write (*,*) "ERROR: Invalid sort direction: ", direction
        stop
    endif
!print *,"hello"
    ! FIND AVAILABLE THREADS
    if (present(nt)) then
        nt1 = nt
    else
        nt1 = 1  ! default to 1 thread in not using openmp
        !$ nt1 = omp_get_max_threads()  ! get max threads available if using openmp
    endif

    multithread: if (array_size < 5 .or. nt1 == 1) then

        ! Single-threaded
        allocate (temp_copy((array_size+1)/2))
        call MergeSort8(A,array_size,temp_copy,(array_size+1)/2)
    else multithread

!!$ call omp_set_nested(.true.)

        ! PARALLEL MERGE SORT
        ! ASSIGN # OF THREADS
!        print *,"nt1",nt1
        nt2 = 2 ** int(log(real(nt1))/log(2.0)) ! get largest power of 2 number of threads (i.e. 2,4,8,16...)
        nt3 = nt2 * groups_per_thread ! make smaller work chunks to increase load balancing
print *,nt2,nt3
        nt_extra = 0
        counter = 1
        levels = log(real(nt3))/log(2.0)
        allocate (s(nt3),f(nt3))
        allocate (done(0:levels-1,nt3))
        done = .false.    ! mark all groups as not done
!        f(0,:) = 0
        allocate (a2(array_size))
!        allocate (available(levels+1),need(levels),counter(levels))
        allocate (next_counter(levels+1))
        allocate (next_counter2(levels+1))
        allocate (order(0:levels,nt3))
        allocate (t1(nt3))

        ! Initialize OpenMP locks
        call omp_init_lock(merge_stack)
        call omp_init_lock(thread_stack)
        allocate (tlock(nt1))
        do i = 1, nt1
            call omp_init_lock(tlock(i))
            flag = omp_test_lock(tlock(i)) ! lock all threads initially for merging
        end do

        if (mod(levels,2) == 0) then
            swap_flag = .true.
        else
            swap_flag = .false.
            ! POINT TO NEW MEMORY MERGESORT PROCEDURE
            if (direction == "A" .or. direction == "a") then
                MergeSort8 => MergeSort8an
            else if (direction == "D" .or. direction == "d") then
                MergeSort8 => MergeSort8dn
            endif
        end if

        print *,"swap_flag",swap_flag,levels
        max_group = anint(real(array_size) / real(nt3)) + 1


call system_clock(count1)
        available = 0
        thread_count = 0
        all_done = .false.
        nullify (head, t_head)
!        go = .false.
        ! SORT PIECES
        ! TO DO: DOUBLE OR QUADRUPLE THREADS TO IMPROVE GRANUALRITY
        ! EVEN BETTER - FIND A AWAY TO NOT WAIT
        ! CAN I PUT SORTED RESULTS INTO NEW MEMORY, NAME THE RESULTS 1...NT
        ! WHEN I HAVE AN EVEN NUMBER THEN I CALL MERGE.
        ! GO UP THE TREE AND WHEN THERE ARE EVEN NUMBER OF AVAILABLE GROUPS MERGE IMMEDIATELY.
        !$omp parallel &
        !$omp default(none) &
        !$omp num_threads(nt1) &
        !$omp private(t, s1, s2, f1, f2, s1_next, s2_next, f1_next, f2_next, level, step, temp_copy, i, j, nt_extra, temp_counter, n, n2, t1,count2, nt4, AtoA2, go, id, id_new, f_next_index) &
        !$omp shared(a, a2, nt2, nt3, array_size, s, f, available, next_counter, next_counter2, need, levels, counter, order, swap_flag, merge_stack, thread_stack, tlock, nt1,count1, done, thread_count, head, tail, t_head, t_tail, all_done, max_group)
        t = omp_get_thread_num() + 1     ! add 1 to make first thread 1 instead of 0
        !$omp do
        do i = 1, nt3
            s(i) = array_size * (i-1) / nt3 + 1     ! start
            f(i) = array_size * i / nt3             ! finish
!            print *,s(i),f(i),i
            call MergeSort8(a(s(i):f(i)), &    ! section to be sorted   ! NEEDS DIFFERENT MERGESORT THAT PUTS RESULTS IN NEW ARRAY
            &               f(i)-s(i)+1, &     ! size of section
            &               a2(s(i):f(i)), &   ! temp or output array, (temp if swap_flag = F, output if swap_flag = T)
            &               f(i)-s(i)+1)       ! size of output array
!
!            call QuickSortA(a(s(i):f(i)), &    ! section to be sorted
!            &               f(i)-s(i)+1, &    ! size of section
!            &               50)     ! Insertion sort limit (50 is a good)
!            if (.not. swap_flag) a2(s(i):f(i)) = a(s(i):f(i))


!            &               left_part)                  ! temp array half the size of section for storing left part during sort
!            done(1,l) = .true.      ! mark finished group as done
!            available(1) = available(1) + 1
!            order(0,available(1)) = i
!            print *,"done",i
            !$omp atomic
            done(0,i) = .true.        ! mark group as finished
            call system_clock(count1)
!            print *,t,count1
            ! Add merge work groups to linked list. ! HOW DO I KNOW I AM GETTING MATCHING PAIRS?  I THINK THERE IS A PROBLEM HERE
            if (mod(i,2) == 0) then
                if (done(0,i-1)) then
                    go = .true.
                    j = -1
                end if
            else
                if (done(0,i+1)) then
                    go = .true.
                    j = 1
                end if
            end if
            if (go) then
!                print *,i
                call omp_set_lock(merge_stack)
                    call add_record(head, tail, &
                    &               min(s(i+j),s(i)), &
                    &               max(s(i+j),s(i)), &
                    &               min(f(i+j),f(i)), &
                    &               max(f(i+j),f(i)), &
                    &               id=i, &
                    &               level=0, &
                    &               AtoA2=swap_flag)
                call omp_unset_lock(merge_stack)
                !$omp atomic
                available = available + 1
                ! SHOULD I ADD THREAD TO WAIT LIST HERE?
!                thread_count = thread_count + 1
!                print *,"here",i,tail%s1,tail%f1,tail%s2,tail%f2,tail%id,tail%level,tail%AtoA2
!                print *,"here2",t,available
                go = .false.
            end if
        end do
        !$omp end do nowait

!        call add_thread(t_head, t_tail, t)
        !$omp critical      ! THIS CAN BE DONE BETTER - I SHOULD USE QUEUE HERE AND PUT WAITING THREADS ON LIST AND RELEASE THEM AS AVAILABLE ONES OCCUR.
        if (available > 0 .and. thread_count < nt2/2) then
            call omp_unset_lock(tlock(t))   ! unlock available thread
            thread_count = thread_count + 1
!            !$omp atomic
!            available = available - 1
        end if
        !$omp end critical
!print *,"thread count",t,available,thread_count,nt2/2

!print *,"here",head%s1,head%f1,head%s2,head%f2,head%id,head%level,head%AtoA2
!$omp barrier
!stop
        ! MERGE
        merge_loop: do
!            print *,"available",t,available,tlock(t)

!            step = 2 ** level
!            if (t > nt3 / step) exit ! release not needed thread

            ! Wait for 2 groups to be available
            i = 1

            ! PROBLEM: SOMETIMES THE FIRST PART OF HIGHER LEVEL ENTERS THE CRITICAL SECTION BEFORE
            ! ANY PARTS ARE BEING DONE RESULTING IN A DEADLOCK.  I WANT ONE THREAD FROM EACH LEVEL ACCESS
            ! AT A TIME - NOT ONLY ONE THREAD TOTAL
!            !$omp critical
!            call omp_set_lock(lock)
            ! IN FUTURE REPLACE SPIN LOCK WITH ARRAY OF LOCKS AND STACK OF AVAILABLE THREAD IDS

            ! SHOULD I CHECK AVAILABLE > 0 HERE AND UNLOCK A THREAD?
     !       print *,1,t,associated(head),available
            call omp_set_lock(tlock(t))   ! unlock available thread
!            !$omp atomic
!            available = available - 1
            if (all_done) exit merge_loop
     !       print *,2,t,associated(head),available
                   call omp_set_lock(merge_stack)
                        if (.not. associated(head)) then
                            print *,"not here", t,level, id, s1, f1, s2, f2, available
                            stop
                        end if

      !                  print *,3,t,associated(head),available
                        s1 = head%s1
                        f1 = head%f1
                        s2 = head%s2
                        f2 = head%f2
                        level = head%level
                        id = head%id
                        AtoA2 = head%AtoA2

!                        print *,"now",head%s1,head%f1,head%s2,head%f2,level,id,AtoA2
                        ! I THINK THE DEAD-LOCK PROBLEM RESULTS FROM THE DELAY BETWEEN RELEASEING A THREAD TO WORK AND THE TIME IT
                        ! TAKES TO REMOVE WORK GROUP FROM QUEUE.  MORE THAN ONE THREAD CAN BE ASSIGNED TO WORK BEFORE GROUP IS REMOVED
                        ! MAYBE I NEED A SHARED FLAG?  CAN I REMOVE HEAD IMMEDIATELY?
                        call remove_head(head)  ! remove merge group from work queue
 ! !$omp end critical
                 call omp_unset_lock(merge_stack)

            if (level == levels) then
                nt4 = nt1
            else if (level == levels - 1) then
                nt4 = max(nt1/2,1)
            else if (level == levels - 2) then
                nt4 = max(nt1/4,1)
            else if (level == levels - 3) then
                nt4 = max(nt1/8,1)
            else
                nt4 = 1
            end if
!            nt4 = max(2**(levels-i+1)/chunks_per_thread,1)                ! threads available for each merge

!            nt4 = 1 ! ***NEED BETTER WAY TO CALCULATE AVAILABLE THREADS
            !$omp atomic
            thread_count = thread_count - nt4
            if (AtoA2) then
!                print *,"sending1:",s1,f1,s2,f2
                    call Merge8a_mt(a(s1:f1), &                 ! left part
                &           f1-s1+1, &                  ! size of left part
                &           a(s2:f2), &                 ! right part
                &           f2-s2+1, &                  ! size of right part
                &           a2(s1:f2), &                ! output array
                &           f2-s1+1, &                  ! size of output array
                &           nt=nt4)           ! number of threads
            else
!                print *,"sending2:",s1,f1,s2,f2
                call Merge8a_mt(a2(s1:f1), &                 ! left part
                &           f1-s1+1, &                  ! size of left part
                &           a2(s2:f2), &                 ! right part
                &           f2-s2+1, &                  ! size of right part
                &           a(s1:f2), &                ! output array
                &           f2-s1+1, &                  ! size of output array
                &           nt=nt4)           ! number of threads
            end if

            ! ALL DONE CHECK
            if (level+1 == levels) then
!                !$omp single
                all_done = .true.
                !$omp flush(all_done)
 !               print *,"all_done",s1,f2
                do i = 1, nt1
!                    PRINT *,t,"unsetting lock",i
                    if (i /= t) call omp_unset_lock(tlock(i))   ! unlock all waiting threads
                end do
!                !$omp end single
                exit merge_loop ! exit when last group is merged
            end if

            ! ! NEED TWO GROUPS TO ADD JOB TO QUEUE - NEED TO CHECK IF PARTNER IS DONE - IF SO THEN ADD TO LIST
            ! add finished flag to array
            id_new = (id+1)/2
            done(level+1,id_new) = .true.
            !$omp flush(done)
            if (mod(id_new,2) == 0) then
                !$omp flush(done)
                if (done(level+1,id_new-1)) then
                    go = .true.
                    j = -1
                    s2_next = s1
                    f2_next = f2
                    f_next_index = (id_new-1) * 2 ** (level+1)
                    f1_next = f(f_next_index)
                    s1_next = s(f_next_index - 2 ** (level+1) + 1)
!                    print *,"a",id_new,s1_next,f1_next
!                    print *,"b",id_new,s2_next,f2_next
                    ! DO I NEED TO TURN OFF DONE FLAG TO PREVENT SECOND MERGE FROM TRIGGERING?
                    ! NEED TO ORDER S1 & S2 SO FIRST IS FIRST
!                    stop
                end if
            else
                !$omp flush(done)
                if (done(level+1,id_new+1)) then
                    go = .true.
                    j = 1
                    s1_next = s1
                    f1_next = f2
!                    print *,"a2",id_new,s1_next,f1_next
                    f_next_index = (id_new+1) * 2 ** (level+1)
                    f2_next = f(f_next_index)
                    s2_next = s(f_next_index - 2 ** (level+1) + 1)
!                    print *,"b2",id_new,s2_next,f2_next
!                    stop
                end if
            end if
!            !$omp end critical

                !$omp critical
            if (go) then
                ! UNSET ONE THREAD
                ! PUT THIS THREAD ON TOP OF WAIT LIST
!                print *,"next",s1_next, &
!                &               f1_next, &
!                &               s2_next, &
!                &               f2_next
                call omp_set_lock(merge_stack)
                    call add_record(head, tail, &       ! DOESN'T WORK -- NEED CALCULATIONS TO FIND BOUNDS
                    &               s1_next, &
                    &               s2_next, &
                    &               f1_next, &
                    &               f2_next, &
                    &               id=id_new, &
                    &               level=level+1, &
                    &               AtoA2=.not. AtoA2)
                call omp_unset_lock(merge_stack)
!                !$omp atomic
                available = available + 1
!                print *,4,t,associated(head),available
                go = .false.
            end if

            ! add thread to wait stack
            call omp_set_lock(thread_stack)
                call add_thread(t_head, t_tail, t)
            call omp_unset_lock(thread_stack)

!            if (available > 0) then

       !         print *,5,t,associated(head),available

!                if (available > 0) then
                if (associated(head)) then
                    if (associated(t_head)) then
                        call omp_set_lock(thread_stack)
        !                    print *,6,t,associated(head),available,t_head%t
                            call omp_unset_lock(tlock(t_head%t))   ! unlock available thread
                            call remove_thread(t_head)
 !                           available = available - 1
                        call omp_unset_lock(thread_stack)
                    else
                        print *,"no threads waiting"
                        stop
                    end if
    !                !$omp atomic
    !                available = available - 1
                end if
                !$omp end critical

!!$omp barrier
!    do j = 1, levels
!    do i = 1, nt2
!    print *,"i,j",i,j,s(i,j),f(i,j)
!    !print *,i,order(:,i)
!    end do
!    end do


        end do merge_loop



        ! USE SYNCH AND FLUSH AND RELEASE TO KEEP THREADS OPEN AND MERGE WHEN 2 GROUPS ARE DONE
        !$omp end parallel

!    do i = 0, levels
!    do j = 1, nt2
!    print "(A,2I3,3I12,I4)","i,j",i,j,s(i,j),f(i,j),f(i,j)-s(i,j)+1,order(i,j)
!    !    print *,i,order(:,i)
!    end do
!    end do
!    print *,"done"

!print *,"Done Sort ",direction
!do i = 1, array_size
!        write (*,"(i7,F15.12,I7)") i,a(i)
!end do


!        ! GET RID OF COPY BY MAKING ORIGINAL MERGESORT GO INTO SAME A OR NEW ARRAY A TO MAKE SURE RESULTS IN IN ARRAY A
!        ! COPY a2 into original array a if necessary (CAN THIS BE DONE INSIDE MAIN PARALLEL BLOCK - I THINK YES)
!        if (swap_flag(levels)) then
!            !$omp parallel &
!            !$omp num_threads(nt1) &
!            !$omp default (none) &
!            !$omp shared (a, a2, nt1, array_size, swap_flag, levels) &
!            !$omp private (t, s2, f2)
!            !$ t = omp_get_thread_num() + 1  ! add 1 to make first thread 1 instead of 0
!            s2 = array_size * (t-1) / nt1 + 1
!            f2 = array_size * t / nt1
!            a(s2:f2) = a2(s2:f2)
!            !$omp end parallel
!        end if

!        print *,a

    endif multithread

end subroutine MSort8e




recursive subroutine MergeSort8ao(A,n,T,n2)

! USED MODULES
use types
use, intrinsic :: iso_fortran_env, only: int64

! DUMMY ARGUMENTS
integer(kind=int64), intent(in) :: n                ! size of array A
type (group), dimension(n), intent(in out) :: A     ! array data to be sorted
integer(kind=int64), intent(in) :: n2               ! size of array T
type (group), dimension(n2), intent(out) :: T       ! temp array that is half the size of A

! LOCAL VARIABLES
integer(kind=int64) :: nA,nT
integer, parameter :: ii = 26
type (group) :: temp
integer(kind=int64) :: i, j

!   if (n < 2) return
!   if (n == 2) then
!      if (A(1)%value > A(2)%value) then
!         v = A(1)
!         A(1) = A(2)
!         A(2) = v
!      endif
!      return
!   endif

    if (n <= ii) then

    ! INSERTION SORT FOR SMALL CHUNK
    do i = 2, n
        j = i - 1
        temp = A(i)
        do !while (j >= 1 .and. a(j)%value > temp%value)
            if (j == 0) exit
            if (a(j)%value <= temp%value) exit
            A(j+1) = A(j)
            j = j - 1
        end do
        a(j+1) = temp
    end do
    return
    end if

   nA=(n+1)/2
   nT=n-nA

   call MergeSort8ao(A,nA,T,n2)
   call MergeSort8ao(A(nA+1:),nT,T,n2)

   if (A(nA)%value > A(nA+1)%value) then
      T(1:nA)=A(1:nA)
      call Merge8a(T,nA,A(nA+1:),nT,A,n)
      ! NEW MEMORY VERSION GET RID OF T AND REPLACE OUTPUT A WITH NEW A2
   endif

end subroutine MergeSort8ao




recursive subroutine MergeSort8do(A,n,T,n2)

! USED MODULES
use types
use, intrinsic :: iso_fortran_env, only: int64

! DUMMY ARGUMENTS
integer(kind=int64), intent(in) :: n                ! size of array A
type (group), dimension(n), intent(in out) :: A     ! array data to be sorted
integer(kind=int64), intent(in) :: n2               ! size of array T
type (group), dimension(n2), intent(out) :: T       ! temp array that is half the size of A

! LOCAL VARIABLES
integer(kind=int64) :: nA,nT
type (group):: v

   if (n < 2) return
   if (n == 2) then
      if (A(1)%value < A(2)%value) then
         v = A(1)
         A(1) = A(2)
         A(2) = v
      endif
      return
   endif
   nA=(n+1)/2
   nT=n-nA

   call MergeSort8do(A,nA,T,n2)
   call MergeSort8do(A(nA+1:),nT,T,n2)

   if (A(nA)%value < A(nA+1)%value) then
      T(1:nA)=A(1:nA)
      call Merge8d(T,nA,A(nA+1:),nT,A,n)
   endif

end subroutine MergeSort8do

! NEW MEMORY VERIONS ARE SLOWER THAN OLD MEMORY VERSIONS.  IT IS FASTER TO COPY RESULTS INTO NEW ARRAY AFTER SORT AND USE OLD MEMORY VERSION TO GET SAME EFFECT AS NEW MEMORY VERSION.
recursive subroutine MergeSort8an(A,n,B,n2)

! USED MODULES
use types
use, intrinsic :: iso_fortran_env, only: int64

! DUMMY ARGUMENTS
integer(kind=int64), intent(in) :: n                 ! size of array A and array B
type (group), dimension(n), intent(in out) :: A      ! array data to be sorted  (serves as temp array in merge)
integer(kind=int64), intent(in) :: n2                ! size of array B
type (group), dimension(n2), intent(out) :: B        ! output array (must be new memory, n2 must equal n)

! LOCAL VARIABLES
integer(kind=int64) :: nA,nB
type (group):: v

    if (n == 1) then
        B(1) = A(1)
        return
    end if
    if (n == 2) then
        if (A(1)%value > A(2)%value) then
            B(1) = A(2)
            B(2) = A(1)
        else
            B(1:2) = A(1:2)
        endif
        return
   endif
   nA=(n+1)/2
   nB=n-nA

   call MergeSort8an(A,nA,B,n2)
   call MergeSort8an(A(nA+1:),nB,B(nA+1:),n2)

   if (B(nA)%value > B(nA+1)%value) then
      A(1:nA)=B(1:nA)
      call Merge8a(A,nA,B(nA+1:),nB,B,n)
   endif

end subroutine MergeSort8an


recursive subroutine MergeSort8dn(A,n,B,n2)

! USED MODULES
use types
use, intrinsic :: iso_fortran_env, only: int64

! DUMMY ARGUMENTS
integer(kind=int64), intent(in) :: n                 ! size of array A and array B
type (group), dimension(n), intent(in out) :: A      ! array data to be sorted  (serves as temp array in merge)
integer(kind=int64), intent(in) :: n2                ! size of array B
type (group), dimension(n2), intent(out) :: B        ! output array (must be new memory, n2 must equal n)

! LOCAL VARIABLES
integer(kind=int64) :: nA,nB
type (group):: v

    if (n == 1) then
        B(1) = A(1)
        return
    end if
    if (n == 2) then
        if (A(1)%value < A(2)%value) then
            B(1) = A(2)
            B(2) = A(1)
        else
            B(1:2) = A(1:2)
        endif
        return
   endif
   nA=(n+1)/2
   nB=n-nA

   call MergeSort8dn(A,nA,B,n2)
   call MergeSort8dn(A(nA+1:),nB,B(nA+1:),n2)

   if (B(nA)%value < B(nA+1)%value) then
      A(1:nA)=B(1:nA)
      call Merge8d(A,nA,B(nA+1:),nB,B,n)
   endif

end subroutine MergeSort8dn


! MULTI-THREAD MERGE
! Divides array a into nt number of parts.  Lookup matching values in array b.  Merges the parts together into new memory array c.
! Ascending sort
subroutine Merge8a_mt(A,nA,B,nB,C,nC,nt)

! USED MODULES
use, intrinsic :: iso_fortran_env, only: int64, real32
use types
use omp_lib, only: omp_get_thread_num

! DUMMY ARGUMENTS
integer (kind=int64), intent(in) :: nA, nB, nC                      ! Size of arrays.  Normal usage: nA+nB = nC
type (group), dimension(nA), intent(in) :: A     ! left part
type (group), dimension(nB), intent(in) :: B     ! right part
type (group), dimension(nC), intent(out) :: C    ! sorted output must be new memory (not overlapping a or b)
integer, intent(in) :: nt                                           ! number of threads

! LOCAL VARIABLES
integer :: t                                             ! thread number
integer (kind=int64), dimension(0:nt) :: dividerA        ! section divider of array a
integer (kind=int64), dimension(0:nt) :: dividerB        ! section divider of array b
integer (kind=int64), dimension(0:nt) :: dividerC        ! section divider of array c
real (kind=real32) :: divider_value                      ! value of section dividers
integer (kind=int64) :: nA_part, nB_part                    ! number of left and right elements of group
integer (kind=int64) :: nC_part                          ! number of sorted elements in group
integer :: i

!print *,"A",a
!print *,"a2",size(a),na
!print *,"B",b
!print *,"b2",size(b),nb
!print *,"nt",nt
t=1
!$ call omp_set_nested(.true.)

    ! initialize first and last divider
    dividerA(0) = 0
    dividerB(0) = 0
    dividerB(nt) = nB
    dividerC(0) = 0
    dividerC(nt) = nC

    !$omp parallel &
    !$omp num_threads(nt) &
    !$omp default(none) &
    !$omp private (t, divider_value, nA_part, nB_part, nC_part) &
    !$omp shared (a, nA, b, nB, c, nC, nt, dividerA, dividerB, dividerC)

    !$ t = omp_get_thread_num() + 1  ! add 1 to make first thread 1 instead of 0
    dividerA(t) = nA * t / nt   ! starting value for dividerA.  Will change if not last in group.
!    print *,"first",t,dividerA(t),a(dividerA(t))%value
    if (t < nt) then
        if (a(dividerA(t))%value == a(dividerA(t)-1)%value) then
            ! Get new DividerA because starting value is in group, and not the last in the group.
            dividerA(t) = LookupAscending(a,nA,a(dividerA(t))%value) - 1
        end if
    end if
    ! POSSIBILITIES
    ! dividerA is zero: Previous groups took all A values (same number extends from one dividerA to the end of A)
    ! dividerA if equal to nA: All values after dividerA starting value are same for the rest of A.
    ! dividerA is between 0 and nA: There are values that exceed the dividerA starting value (normal operation)
!    print *,"second",t,dividerA(t),a(dividerA(t))%value
    divider_value = a(dividerA(t))%value
    if (t < nt) then
        ! find closest match that is just less than divider_value in array b
        dividerB(t) = LookupAscending(b,nB,divider_value) - 1
        dividerC(t) = dividerA(t) + dividerB(t)
    end if
    !$omp barrier   ! make sure all divider values are calculated because each thread needs previous thread's divider values
!print "(i2,6I8)", t,dividera(t),dividerb(t),dividerc(t),nA,nB,nC
!print "(i2,1x,F11.8)", t,divider_value
!$omp barrier
    nA_part = dividerA(t) - (dividerA(t-1)+1) + 1
    nB_part = dividerB(t) - (dividerB(t-1)+1) + 1
    nC_part = dividerC(t) - (dividerC(t-1)+1) + 1
!print *,t,nA_part,nB_part,nC_part
!!$omp barrier

! POSIBILITIES
! 1. nA_part == 0: Caused by dividerA being equal to nA.  This happens when all values in A beyond starting value of dividerA are
!    equal.  After first thread, copy none of A-part and all of B-part into C.  First thead does normal (3) operation with all of A
!    and a part of B (nA_part is not equal to 0 for the first thread, only for all threads greater than one)
! 2. nB_part == 0: Happens if Lookup finds no value in B that is less than divider_value in A.  Copy A-part into C.
! 3. Neither nA_part or nB_part are equal to 0.  Merge A-part and B-part (normal operation).

! BOTTOM OPTIONS SHOULD NOT BE ISSUES
! 4. Could there be a situation when there is no values in b?  If so, then just copy A into C.
! 5. Could both parts be empty?  I think so.  Would it be a problem?

    if (nA_part == 0) then ! possibility 1
        if (nB_part > 0) C(dividerC(t-1)+1:dividerC(t-1)+nB_part) = B(dividerB(t-1)+1:dividerB(t)) ! copy only B-part
!    else if (dividerB(t) == nB) then ! possibility 2
!        print *,"possibility 2",t,dividerC(t-1)+1+nB,dividerC(t),dividerC(t-1)+1,dividerC(t-1)+nB
!        c(dividerC(t-1)+1+nB:dividerC(t)) = a(dividerA(t-1)+1:dividerA(t))  ! copy all of A FIND OUT IF THIS HAPPENS - COPY ALL OF ONE AND PART OF THE OTHER.
! I SHOULD DO IT HERE INSTEAD OF SENDING THAT TO THE MERGE - FASTER. I THINK IT CAN ONLY HAPPEN FOR DESCENDING SORT.  NEEDS TESTING.
! I THINK THIS COULD HAPPEN NOW.  STILL NEEDS TESTING.  DOESN'T WORK - SECOND GROUP NEEDS MERGE
!        c(dividerC(t-1)+1:dividerC(t-1)+nB) = b                             ! copy all of B
    else if (nB_part == 0) then
        C(dividerC(t-1)+1:dividerC(t)) = A(dividerA(t-1)+1:dividerA(t)) ! copy only A-part
    else
        call Merge8a(   A(dividerA(t-1)+1:dividerA(t)), nA_part, &   ! A-part
        &               B(dividerB(t-1)+1:dividerB(t)), nB_part, &   ! B-part
        &               C(dividerC(t-1)+1:dividerC(t)), nC_part)     ! sorted part

    end if
    !$omp end parallel

end subroutine Merge8a_mt

! MERGESORT for group type list (ascending and descending)
! Ascending merge (merges 2 ascending sorted lists into 1 ascending sorted list)
subroutine Merge8a(a,nA,b,nB,c,nC)

! USED MODULES
use types
use, intrinsic :: iso_fortran_env, only: int64

! DUMMY ARGUMENTS
integer(kind=int64), intent(in) :: nA, nB, nC         ! Size of arrays.  Normal usage: nC = nA+nB
!   type (group), dimension(:), intent(in) :: A ! Make sure A is a copy of any subset of C or sort overwrites as it goes
!   type (group), dimension(:), intent(in) :: B        ! B overlays C(nA+1:nC)
!   type (group), dimension(:), intent(out) :: C
!    ! ***NOTE: USING EXPLICIT ARRAY SIZES AS SHOW BELOW SOMETIMES CAUSES SEGMENTATION FAULTS WITH LARGE ARRAY SIZES
!   automatic arrays are faster than assumed size arrays
type (group), dimension(nA), intent(in) :: a  ! left part
type (group), dimension(nB), intent(in) :: b  ! right part
type (group), dimension(nC), intent(out) :: c ! output array
! Note: for single-threaded merge, such as the call from MergeSort, array a is a copy of the left part of array c.  Also, B overlays
! array c(nA+1:nC).  As multi-threaded merge array a and b are parts of the same input array.  Multi-threaded usage also requires
! array c to be different memory not overlapping arrays a or b.

! LOCAL VARIABLES
integer(kind=int64) :: i, j, k

    i = 1; j = 1; k = 1
    do
        if (i > nA .or. j > nB) exit
        if (a(i)%value <= b(j)%value) then
            c(k) = a(i)
            i = i + 1
        else
            c(k) = b(j)
            j = j + 1
        endif
        k = k + 1
    enddo
    if (i <= nA) then
        c(k:) = a(i:)
        return
    endif
    if (j <= nB) c(k:) = b(j:)    ! This statement is only necessary for multi-threaded merge

end subroutine Merge8a


! descending sort        detailed_result_copy(106:110)%value = detailed_result_copy(5)%value

subroutine Merge8d_mt(a,nA,b,nB,c,nC,nt)

! USED MODULES
use, intrinsic :: iso_fortran_env, only: int64, real32
use types
use omp_lib, only: omp_get_thread_num

! DUMMY ARGUMENTS
integer (kind=int64), intent(in) :: nA, nB, nC                      ! Size of arrays.  Normal usage: nA+nB = nC
type (group), dimension(nA), intent(in) :: a     ! left part
type (group), dimension(nB), intent(in) :: b     ! right part
type (group), dimension(nC), intent(out) :: c    ! sorted output must be new memory (not overlapping a or b)
integer, intent(in) :: nt                                           ! number of threads

! LOCAL VARIABLES
integer :: t                                             ! thread number
integer (kind=int64), dimension(0:nt) :: dividerA        ! section divider of array a
integer (kind=int64), dimension(0:nt) :: dividerB        ! section divider of array b
integer (kind=int64), dimension(0:nt) :: dividerC        ! section divider of array c
real (kind=real32) :: divider_value                      ! value of section dividers
integer (kind=int64) :: nA_part, nB_part                    ! number of left and right elements of group
integer (kind=int64) :: nC_part                          ! number of sorted elements in group

    ! initialize first and last divider
    dividerA(0) = 0
    dividerB(0) = 0
    dividerB(nt) = nB
    dividerC(0) = 0
    dividerC(nt) = nC
t=1
!$ call omp_set_nested(.true.)

    !$omp parallel &
    !$omp num_threads(nt) &
    !$omp default(none) &
    !$omp private (t, divider_value, nA_part, nB_part, nC_part) &
    !$omp shared (a, nA, b, nB, c, nC, nt, dividerA, dividerB, dividerC)
    !$ t = omp_get_thread_num() + 1   ! add 1 to make first thread 1 instead of 0
    dividerA(t) = nA * t / nt   ! starting value for dividerA.  Will change if not last in group.
!    print *,"first",t,dividerA(t)
    if (t < nt) then
        if (a(dividerA(t))%value == a(dividerA(t)+1)%value) then
            ! Get new DividerA because starting value is in group, and not the last in the group.
            dividerA(t) = min(LookupDescending(a,nA,a(dividerA(t))%value),nA)
!                print *,"second",t,dividerA(t)
        end if
    end if
    ! POSSIBILITIES  - NEED TO MAKE SURE THESE DESCRIPTIONS WORK FOR DECENDING
    ! dividerA is zero: Previous groups took all A values (same number extends from one dividerA to the end of A)
    ! dividerA if equal to nA: All values after dividerA starting value are same for the rest of A.
    ! dividerA is between 0 and nA: There are values that exceed the dividerA starting value (normal operation)
    divider_value = a(dividerA(t))%value
    if (t < nt) then
        ! find closest matching in array b that is just greater than divider_value
        dividerB(t) = LookupDescending(b,nB,divider_value)
        dividerC(t) = dividerA(t) + min(dividerB(t),nB)
    end if
    !$omp barrier   ! make sure all divider values are calculated
!print "(i2,6I9)", t,dividera(t),dividerb(t),dividerc(t),nA,nB,nC
!print "(i2,F11.8)", t,divider_value
    nA_part = dividerA(t) - (dividerA(t-1)+1) + 1
    nB_part = dividerB(t) - (dividerB(t-1)+1) + 1
    nC_part = dividerC(t) - (dividerC(t-1)+1) + 1
!print "(i2,6I8)", t,nA_part,nB_part,nC_part

!$omp barrier

!!$omp critical

! POSIBILITIES  - NEED TO MAKE SURE THESE DESCRIPTIONS WORK FOR DECENDING
! 1. nB_part > nB: Caused by no values in B being greater than dividerA.  Both parts of A and B are already in descending order.
!    Copy all of B into C and then copy the A-part into C.
! 2. nB_part <= 0: Caused by situation 1 occuring on a different thread.  All of B has been copied already into C.  Just copy the
!    A-part into C.
! 3. Normal operation.  There is
! WHAT ABOUT BELOW CASES?  THEY NEED TO BE CONSIDERED.
! 1. nA_part == 0: Caused by dividerA being equal to nA.  This happens when all values in A beyond starting value of dividerA are
!    equal.  After first thread, copy none of A-part and all of B-part into C.  First thead does normal (3) operation with all of A
!    and a part of B (nA_part is not equal to 0 for the first thread, only for all threads greater than one)
! 2. nB_part == 0: Happens if Lookup finds no value in B that is less than divider_value in A.  Copy A-part into C.
! 3. Neither nA_part or nB_part are equal to 0.  Merge A-part and B-part (normal operation).

    if (nB_part > nB) then
        C(dividerC(t-1)+1:dividerC(t-1)+nB) = A(dividerA(t-1)+1:dividerA(t))  ! copy A-part
        C(dividerC(t-1)+1+nB:dividerC(t)) = B                                 ! copy all of B
    else if (nB_part <= 0) then
        if (nA_part > 0) C(dividerC(t-1)+1:dividerC(t)) = A(dividerA(t-1)+1:dividerA(t)) ! copy all of array a because no values in array B
    else if (nA_part == 0) then ! possibility 1
        if (nB_part > 0) C(dividerC(t-1)+1:dividerC(t-1)+nB_part) = b(dividerB(t-1)+1:dividerB(t)) ! copy only B-part
    else
!    print "(A,I2,3I8)","TT",t,dividerA(t-1)+1,dividerA(t),nA,size(a)
        call Merge8d2(   A(dividerA(t-1)+1:dividerA(t)), nA_part, &    ! left
        &               B(dividerB(t-1)+1:dividerB(t)), nB_part, &   ! right
        &               C(dividerC(t-1)+1:dividerC(t)), nC_part)    ! sorted
    end if
!    !$omp end critical
    !$omp end parallel

end subroutine Merge8d_mt

! Descending merge (merges 2 descending sorted lists into 1 descending sorted list)
subroutine Merge8d(a,nA,b,nB,c,nC)

! USED MODULES
use types
use, intrinsic :: iso_fortran_env, only: int64

! DUMMY ARGUMENTS
integer(kind=int64), intent(in) :: nA, nB, nC         ! Size of arrays.  Normal usage: nC = nA+nB
type (group), dimension(nA), intent(in) :: a
type (group), dimension(nB), intent(in) :: b
type (group), dimension(nC), intent(out) :: c
! Note: for single-threaded merge, such as the call from MergeSort, array a is a copy of the left part of array c.  Also, B overlays
! array c(nA+1:nC).  As multi-threaded merge array a and b are parts of the same input array.  Multi-threaded usage also requires
! array c to be different memory not overlapping arrays a or b.

! LOCAL VARIABLES
integer(kind=int64) :: i, j, k

    i = 1; j = 1; k = 1
    do
        if (i > nA .or. j > nB) exit
        if (a(i)%value >= b(j)%value) then
            c(k) = a(i)
            i = i + 1
        else
            c(k) = b(j)
            j = j + 1
        endif
        k = k + 1

    enddo
    if (i <= nA) then
        c(k:) = a(i:)
        return
    endif
    if (j <= nB) c(k:) = b(j:)    ! THIS STATEMENT IS NOT NECESSARY IN SINGLE THREADED MERGE

end subroutine Merge8d

! Descending merge (merges 2 descending sorted lists into 1 descending sorted list)
subroutine Merge8d2(a,nA,b,nB,c,nC)

! USED MODULES
use types
use, intrinsic :: iso_fortran_env, only: int64

! DUMMY ARGUMENTS
integer(kind=int64), intent(in) :: nA, nB, nC         ! Size of arrays.  Normal usage: nC = nA+nB
type (group), dimension(nA), intent(in) :: a
type (group), dimension(nB), intent(in) :: b
type (group), dimension(nC), intent(out) :: c
! Note: for single-threaded merge, such as the call from MergeSort, array a is a copy of the left part of array c.  Also, B overlays
! array c(nA+1:nC).  As multi-threaded merge array a and b are parts of the same input array.  Multi-threaded usage also requires
! array c to be different memory not overlapping arrays a or b.

! LOCAL VARIABLES
integer(kind=int64) :: i, j, k
integer(kind=int64) :: z, z2

    i = 1; j = 1; k = 1
    do
        if (i > nA .or. j > nB) exit
        if (a(i)%order == 11746939 .or. a(i)%order == 3981213 .or. b(j)%order == 11746939 .or. b(j)%order == 3981213) then
print "(A,4I8,1X,F15.13,I8)","a ",nA,i,j,k,a(i)
if (k>1) print "(A,4I8,1X,F15.13,I8)","last",c(k-1)
!print "(A,4I8,1X,F15.13,I8)","b ",nB,i,j,k,b(j)
end if

        if (a(i)%value >= b(j)%value) then
            c(k) = a(i)
            i = i + 1
        else
            c(k) = b(j)
            j = j + 1
        endif
!        if (c(k)%value == 1.001708984375) then
!        print "(A,4I8,1X,F15.13,I8)","a ",nA,i,j,k,c(k)
!        end if
        write (42,"(A,3I8,1X,F15.13,I8)") "c ",i,j,k,c(k)

        k = k + 1

    enddo
!    if (i <= nA) then
!    if (abs(a(i)%value - 1.001708984376) < .0001) then
!    end if
    if (i <= nA) then
        do z = i, nA
        write (42,"(A,3I8,1X,F15.13,I8)") "i<=nA ",z,j,k,a(z)
!            if (a(z)%order == 11746939 .or. a(z)%order == 3981213) then
!                print *,"here3",i,j,k
!                print *,"here3",nA,nB,nC
!            end if
        end do
        c(k:) = a(i:)
        return
    endif
    if (j <= nB) then
        do z = j, nB
        write (42,"(A,3I8,1X,F15.13,I8)") "j<=nB ",z,j,k,b(z)
        end do
    c(k:) = b(j:)    ! THIS STATEMENT IS NOT NECESSARY IN SINGLE THREADED MERGE
    end if

end subroutine Merge8d2


! find first location where value is exceeded in array a.  Array a must be sorted in ascending order.  Uses binary search.
function LookupAscending(a,array_size,value)

! USED MODULES
use, intrinsic :: iso_fortran_env, only: int64, real32
use types

! DUMMY ARGUMENTS
integer (kind=int64) :: LookupAscending
integer (kind=int64), intent(in) :: array_size         ! Size of a
type (group), dimension(array_size), intent(in) :: a
real (kind=real32), intent(in) :: value

! LOCAL VARIABLES
integer (kind=int64) :: half ! half the distance between binary searches

    if (a(1)%value > value) then
        LookupAscending = 1    ! first value exceeds
    else
        if (a(array_size)%value <= value) then
            LookupAscending = array_size + 1 ! last value is too small
        else    ! value is in between.  Find it.
            LookupAscending = (array_size+1)/2    ! starting location in middle
            half = LookupAscending
            do
                half = (half+1) / 2
                if (a(LookupAscending)%value > value) then
                    if (a(LookupAscending-1)%value <= value) then
                        exit    ! found
                    else
                        LookupAscending = max(LookupAscending - half, 1) ! move down half
                    endif
                else
                    LookupAscending = min(LookupAscending + half, array_size) ! move up half
                endif
            enddo
        endif
    endif

end function LookupAscending

! find last location where value is not exceeded in array a.  Array a must be sorted in descending order.  Uses binary search.
! group type array
function LookupDescending(a,array_size,value)

! USED MODULES
use, intrinsic :: iso_fortran_env, only: int64, real32
use types

! DUMMY ARGUMENTS
integer (kind=int64) :: LookupDescending
integer (kind=int64), intent(in) :: array_size         ! Size of a
type (group), dimension(array_size), intent(in) :: a
real (kind=real32), intent(in) :: value

! LOCAL VARIABLES
integer (kind=int64) :: half ! half the distance between binary searches

    if (a(1)%value < value) then
        LookupDescending = 1    ! first value is too small
    else
        if (a(array_size)%value >= value) then
            LookupDescending = array_size + 1 ! last value is too big
        else    ! value is in between.  Find it.
            LookupDescending = (array_size+1)/2    ! starting location in middle
            half = LookupDescending
            do
                half = (half+1) / 2
                if (a(LookupDescending)%value < value) then
                    if (a(LookupDescending-1)%value >= value) then
                        LookupDescending = LookupDescending - 1 ! go down one value to get last one that doesn't exceed value
                        exit    ! found
                    else
                        LookupDescending = max(LookupDescending - half, 1) ! move down half
                    endif
                else
                    LookupDescending = min(LookupDescending + half, array_size) ! move up half
                endif
            enddo
        endif
    endif

end function LookupDescending




!***********************************************************************************************************************************
!*                                                      ADD RECORD INTO LINKED LIST                                                *
!*                                                                                                                                 *
!* Puts record into linked list.                                                                                                   *
!***********************************************************************************************************************************
subroutine add_record(head, tail, s1, s2, f1, f2, id, level, AtoA2)
use, intrinsic :: iso_fortran_env, only: int64
! type queue is in this module

! DUMMY ARGUMENTS
type (queue), intent(in out), pointer :: head               ! head of linked list
type (queue), intent(in out), pointer :: tail               ! tail of linked list
integer (kind=int64), intent(in) :: s1, s2, f1, f2
integer, intent(in) :: id
integer, intent(in) :: level
logical, intent(in) :: AtoA2

    if (.not. associated(head)) then
        allocate(head)
        tail => head
        nullify (tail%next)
    else
        allocate(tail%next)
        tail => tail%next
        nullify (tail%next)
    endif
    ! put data into linked list
    tail%s1 = s1
    tail%s2 = s2
    tail%f1 = f1
    tail%f2 = f2
    tail%id = id
    tail%level = level
    tail%AtoA2 = AtoA2

end subroutine add_record

!***********************************************************************************************************************************
!*                                                      REMOVE HEAD OF LINKED LIST
!*
!* Deallocates head record of linked list and moves head to next record, if one exists.
!***********************************************************************************************************************************
subroutine remove_head(head)
! type queue is in this module

! DUMMY ARGUMENTS
type (queue), intent(in out), pointer :: head               ! head of linked list

! LOCAL VARIABLES
type (queue), pointer :: next
type (queue), pointer :: temp

    next => head%next
    temp => head
    if (associated(next)) then
        head => next    ! point to next record
    else
        nullify (head)  ! nullify if no more records
    end if
    deallocate(temp)

end subroutine remove_head

!***********************************************************************************************************************************
!*                                                      ADD RECORD INTO LINKED LIST                                                *
!*                                                                                                                                 *
!* Puts record into linked list.                                                                                                   *
!***********************************************************************************************************************************
subroutine add_thread(head, tail, t)!, lock)
use, intrinsic :: iso_fortran_env, only: int64
use omp_lib, only: omp_lock_kind
! type queue is in this module

! DUMMY ARGUMENTS
type (stack), intent(in out), pointer :: head               ! head of linked list
type (stack), intent(in out), pointer :: tail               ! tail of linked list
integer, intent(in) :: t
!integer (kind=omp_lock_kind), intent(in out) :: lock

    if (.not. associated(head)) then
        allocate(head)
        tail => head
        nullify (tail%next)
    else
        allocate(tail%next)
        tail => tail%next
        nullify (tail%next)
    endif
    ! put data into linked list
    tail%t = t
!    call omp_unset_lock(lock)

end subroutine add_thread

!***********************************************************************************************************************************
!*                                                      REMOVE HEAD OF LINKED LIST
!*
!* Deallocates head record of linked list and moves head to next record, if one exists.
!***********************************************************************************************************************************
subroutine remove_thread(head)!, lock)
use omp_lib, only: omp_lock_kind
! type queue is in this module

! DUMMY ARGUMENTS
type (stack), intent(in out), pointer :: head               ! head of linked list
!integer (kind=omp_lock_kind), intent(in out) :: lock

! LOCAL VARIABLES
type (stack), pointer :: next
type (stack), pointer :: temp

!    call omp_set_lock(lock)
    next => head%next
    temp => head
    if (associated(next)) then
        head => next    ! point to next record
    else
        nullify (head)  ! nullify if no more records
    end if
    deallocate(temp)

end subroutine remove_thread




end module sort




! PROGRAM TO TEST SORT MODULE
program test_sort
! USED MODULES
use types
use sort

implicit none

integer (kind=int64), parameter :: nA =  100000000
type (group), allocatable, dimension(:) :: A
integer :: count1, count2, rate
integer, dimension(12) :: seed = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]

    call random_seed(put = seed)
    allocate (A(nA))
    call make_data(A,nA)
    call system_clock(count1,rate)
!    call MTSort(A,nA,"Ascending")   ! Missing optional 4th argument means use all available threads.  To specify, add 4th argument.
call Msort8e(a,nA,"Ascending",4,64)
    call system_clock(count2)

    write (*,*) "First and last in sorted list"
    write (*,*) A(1), A(nA)
    write (*,*) "Execution time in seconds:"
    write (*,*) real(count2-count1)/real(rate)

contains

subroutine make_data(A,nA)
! USED MODULES
use, intrinsic :: iso_fortran_env, only: int64
use types

! DUMMY ARGUMENTS
integer (kind=int64), intent(in) :: nA
type (group), dimension(nA), intent(out) :: A

! LOCAL VARIABLES
integer (kind=int64) :: i
real :: random

    do i = 1, nA
        call random_number(random)
        A(i)%value = random
        A(i)%order = i
    end do

end subroutine make_data

end program test_sort
